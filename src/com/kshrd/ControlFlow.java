package com.kshrd;

import java.util.Scanner;

public class ControlFlow {

public static void main(String arg[]){

    Scanner input = new Scanner(System.in) ;
    System.out.print("Enter a : ");
    int a = input.nextInt();

    System.out.print("Enter b : ");
    int b = input.nextInt();

    System.out.print("Enter c : ");
    int c = input.nextInt();
 int max=0;

 // int c ;  ( a, b , c ) if else or ternary find max
 // find max(a,b)
//    if (a>b) max = a;
//    else max =b;

    // find max ( a, b ,c )

//    if (a>b && a>c) max = a;
//    else if (b>a && b>c ) max = b;
//    else max =c;
 // findmax(a,b,c)                            // (b>a)
max = (a>b)? (a>c)? a:c     :    (b>c)? b : c;


 //max = (a>b)? a : b;  // ternary operator

    System.out.println(" Max value is : "+ max);

    // findmax(a,b,c,d) // use ternary operator


}

}
