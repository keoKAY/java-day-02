package com.kshrd;
/*
 *       1. Math
 *       2. Physics
 *       3. Java ------> max 100
 *       4. Student Name: firstname lastname ex. Dara Kok
 *       5. Class : A1, A3, A3
 *       Print -> Student name , Class name ,  SUM, AVG, Grade
 *
 *       if avg >=85 grade A
 *           avg >=75 grade B
 *           avg >=65 grade C
 *           avg >=50 grade D
 *           avg <50  grade F
 *
 *
 * */

import java.util.Scanner;

public class InputOutputPractice {

    public static void  main(String[] args){

        Scanner input = new Scanner(System.in);
        // String

        System.out.print("Physic Score : ");
        int physic = input.nextInt();
//        System.out.println();

        System.out.print("Math Score : ");
        int math = input.nextInt();
//        System.out.println();

        System.out.print("Java Score: ");
        int java = input.nextInt();//12


        input.nextLine(); // cleanup the useless space or buffer
//        System.out.println();

        System.out.print("Enter Student Name : ");
        String studentName=input.nextLine();
//        System.out.println();

        System.out.print("Enter Class Name: ");
        String className=input.next();

        int sum = physic+math+java;
        float avg =(float) sum/3;

        String grade="";

        if (avg >=85) grade="A";
         else if  (avg >=75)    grade="B";
         else if (avg >=65) grade="C";
         else if (avg >=50)  grade="D";

         else grade= "F";


         // output
        System.out.println("Student Name : "+studentName);
        System.out.println("Class Name : "+className);
        System.out.println("Total Score : "+sum);
        System.out.println("Average : "+avg);
        System.out.println("Grade : "+grade);


    }
}
